import './css/style.scss'
import { library, dom } from '@fortawesome/fontawesome-svg-core'
import { faCogs, faMitten, faShoppingCart } from '@fortawesome/free-solid-svg-icons'

library.add(faCogs, faMitten, faShoppingCart);
dom.watch();

document.getElementById('burger').onclick = (function() {
  let navBar = document.getElementById('nav-bar');
  let header = document.getElementById('header');
  return function() {
    for (let child of this.children) {
      child.classList.toggle('burger-opened');
    }
    for (let elem of [navBar, document.body, header]) {
      elem.classList.toggle('flex-nav-opened');
    }
  }
})()
